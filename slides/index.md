---
title: PHP Static Analysis 101
description: An introduction to what's behind PHP static code analysis
url: https://beram-presentation.gitlab.io/php-static-analysis-101
image: https://beram-presentation.gitlab.io/php-static-analysis-101/assets/metadata_image.png
theme: basic
class:
    - lead
    - invert
paginate: true
footer: |
    @rambaud_b :pig: :heart: :guitar: :musical_note:
    https://gitlab.com/beram-presentation/php-static-analysis-101
emoji:
    shortcode: true
---

![bg right](assets/elephpant.jpeg)

# **PHP Static Code Analysis 101**

An introduction to what's behind PHP static code analysis

:elephant: :elephant: :elephant:

<!--
Just an intro, other parts later
-->

---

# Hello! I'm beram :wave:

![bg right auto](assets/avatar.png)

:pig: Benjamin Rambaud
:elephant: PHP Engineer at [@ekino_France](https://twitter.com/ekino_france)

:unicorn:

- Twitter: [@rambaud_b](https://twitter.com/rambaud_b)
- Gitlab: [beram](https://gitlab.com/users/beram/groups)
- Github: [brambaud](https://github.com/brambaud)
- Blog: https://brambaud.github.io/
<!-- - Drupal: [beram](https://drupal.org/u/beram) -->

---

# What is Static Code Analysis:question:

---

# Code Analysis:question:

<!-- Analysis: process as a method of studying the nature of something or of determining its essential features and their relations -->

> Process of automatically analyzing the behavior of computer programs regarding a property such as correctness, robustness, safety and liveness.

[Thanks Wikipedia :wink:](https://en.wikipedia.org/wiki/Program_analysis)

---

# Static:question:

<!--
Static analysis is a process for determining
the relevant properties of a (PHP) program
without actually executing the program.

Dynamic analysis is a process for
determining the relevant properties of a
program by monitoring/observing the
execution states of one or more
runs/executions of the program.

Computing the code coverage according to a test suite is a standard dynamic analysis technique.

Why static?
Quicker, no need to have the full app running (no DB needed etc..)
-->

| | **_Static_** | **_Dynamic_**
| --- | --- | ---
| **_Execute the program?_** | No | Yes
| **_Examples_** | Bug Finder</br> Coding Standards</br> etc.. | Code Coverage</br> Security</br> etc..

---

# PHP Static Analysis Tools ❓

---

# PHP Static Analysis Tools

- [PHP](https://www.php.net/) :elephant: :smile:
- [PHPStan](https://github.com/phpstan/phpstan)
- [Psalm](https://github.com/vimeo/psalm)
- [Phan](https://github.com/phan/phan)
- [PHP Mess Detector (PHPMD)](https://phpmd.org/)
- [PHP Copy/Paste Detector (PHPCPD)](https://github.com/sebastianbergmann/phpcpd)
- [PHP_CodeSniffer (PHPCS)](https://github.com/squizlabs/PHP_CodeSniffer)
- [PHP CS Fixer](https://github.com/FriendsOfPHP/PHP-CS-Fixer)
- etc..

More at https://github.com/exakat/php-static-analysis-tools

---

# How to do Static Analysis:question:

- Regular Expressions 😰
- Tokens  👈 :elephant:
- Abstract Syntax Tree (AST)  👈 :elephant:

---

# Overview of How PHP Works

```text
             -------   Tokens   --------    AST    ----------   Opcodes   -----------------
PHP Code -->| Lexer |--------->| Parser |-------->| Compiler |---------->| VM -> Execution |
             -------            --------           ----------             -----------------
```

<style scoped>
    .code-little {
      font-size: 0.6em;
      display: flex;
    }
</style>

<div class="code-little">

<div style="flex: auto;">

Starting from:

```php
<?php

// Chookity
echo "I love ponies";
```

</div>

<div style="flex: auto;">

To execute the opcodes:
```php

000 ECHO string("I love ponies")
001 RETURN int(1)

```

</div>
</div>

<!--
PHP process one file at a time. We are going to do the same for this talk.

0000 ECHO string("I love ponies")
0001 RETURN int(1)

To get opcode you could use Vulcan Logic Disassembler: http://pecl.php.net/package/vld
-->

---

# Tokens.. Lexer..:question: 🤔

- **_Lexical analysis, lexing or tokenization_**
    > process of converting a sequence of characters into a sequence of tokens.

- **_Tokens_**
    > strings/symbols with an assigned and thus identified meaning.

[Thanks Wikipedia :wink:](https://en.wikipedia.org/wiki/Lexical_analysis)

---

# Tokens.. Lexer..:question: 🤔

For instance this sentence in English is composed of tokens.

* Word (`instance`)
* Whitespace (` `)
* full stop (`.`)

<!--
A lexeme is a sequence of characters in the source program that matches the pattern for a token and is identified by the lexical analyzer as an instance of that token.
Some authors term this a "token", using "token" interchangeably to represent the string being tokenized, and the token data structure resulting from putting this string through the tokenization process.
The word lexeme in computer science is defined differently than lexeme in linguistics.
A lexeme in computer science roughly corresponds to a word in linguistics, although in some cases it may be more similar to a morpheme.
-->

---

# Tokens in PHP

- `T_COMMENT`: `//` or `#`, and `/* */`
- `T_CONST`: `const`
- `T_ELSE`: `else`
- `T_FOREACH`: `foreach`
- `T_FUNCTION`: `function`
- `T_IS_EQUAL`: `==`
- `T_IS_IDENTICAL`: `===`
- `T_VARIABLE`: `$foo`
- etc..

See [List of Tokens](https://www.php.net/manual/en/tokens.php)

---

# Tokens in PHP:

`token_get_all()` returns an array of tokens. Each token is either:

- a single character (i.e. `;`, `.`, `>`, `!`, etc...)
- a three element array containing in element:
    - `0`: the token index
    - `1`: the string content of the original token
    - `2`: the line number

<span style="font-size:50%; margin-top: 2em">
See:
<a href="https://www.php.net/manual/en/function.token-get-all.php">token_get_all() manual</a>
or
<a href="https://www.php.net/manual/en/function.token-name.php">token_name() manual</a>
or 
<a href="https://www.php.net/manual/en/tokens.php">List of Tokens</a>
</span> 

---

<!-- _footer: @rambaud_b :pig: :heart: :guitar: :musical_note: https://gitlab.com/beram-presentation/php-static-analysis-101 -->

<style scoped>
    code {
      font-size: 0.6em;
    }
</style>

```php
<?php \var_dump(\token_get_all('<?php echo "I love ponies";', TOKEN_PARSE));
```
```text
array(5) {
  [0]=>
  array(3) {
    [0]=>
    int(382)
    [1]=>
    string(6) "<?php "
    [2]=>
    int(1)
  }
  [1]=>
  array(3) {
    [0]=>
    int(324)
    [1]=>
    string(4) "echo"
    [2]=>
    int(1)
  }
  [2]=>
  array(3) {
    [0]=>
    int(385)
    [1]=>
    string(1) " "
    [2]=>
    int(1)
  }
  [3]=>
  array(3) {
    [0]=>
    int(315)
    [1]=>
    string(15) ""I love ponies""
    [2]=>
    int(1)
  }
  [4]=>
  string(1) ";"
}
```

---

# Tokens in PHP

```php
<?php

$src = '<?php echo "I love ponies";';
$tokens = \token_get_all($src, TOKEN_PARSE);

foreach ($tokens as $token) {
    if (\is_array($token)) {
        \printf('Line %d: %s (%s) (%s)%s', $token[2], \token_name($token[0]), $token[0], $token[1], PHP_EOL);
        continue;
    }
    
    \printf('String token: %s%s', $token, PHP_EOL);
}
```

```text
Line 1: T_OPEN_TAG (382) (<?php )
Line 1: T_ECHO (324) (echo)
Line 1: T_WHITESPACE (385) ( )
Line 1: T_CONSTANT_ENCAPSED_STRING (315) ("I love ponies")
String token: ;
```

---

# Tokens in PHP

But... wait since PHP 8.0 the
[PHP RFC Object-based token_get_all() alternative](https://wiki.php.net/rfc/token_as_object)
has been accepted
🥰🥰🥰

---

# Tokens in PHP

`PhpToken::tokenize()` returns an array of `PhpToken` objects 🎉

```php
PhpToken {
    public int $id;
    public string $text;
    public int $line;
    public int $pos;
    
    final public __construct ( int $id , string $text , int $line = -1 , int $pos = -1 )
    public getTokenName ( ) : string|null
    public is ( int|string|array $kind ) : bool
    public isIgnorable ( ) : bool
    public __toString ( ) : string
    public static tokenize ( string $code , int $flags = 0 ) : array
}
```

<span style="font-size:50%; margin-top: 2em">
See:
<a href="https://www.php.net/manual/fr/class.phptoken.php">PhpToken manual</a>
</span>

---

# Tokens in PHP

```php
<?php

$src = '<?php echo "I love ponies";';

$tokens = \PhpToken::tokenize($src, TOKEN_PARSE);
foreach ($tokens as $token) {
    \printf('Line %d: %s (%s) (%s)%s', $token->line, $token->getTokenName(), $token->id, $token->text, PHP_EOL);
}
```

```text
Line 1: T_OPEN_TAG (390) (<?php )
Line 1: T_ECHO (326) (echo)
Line 1: T_WHITESPACE (393) ( )
Line 1: T_CONSTANT_ENCAPSED_STRING (318) ("I love ponies")
Line 1: ; (59) (;)
```

---

# Tokens in PHP

```php
$tokens = \token_get_all($src, TOKEN_PARSE);
foreach ($tokens as $token) {
    if (\is_array($token)) {
        \printf('Line %d: %s (%s) (%s)%s', $token[2], \token_name($token[0]), $token[0], $token[1], PHP_EOL);
        continue;
    }
    
    \printf('String token: %s%s', $token, PHP_EOL);
}
```

VS

```php
$tokens = \PhpToken::tokenize($src, TOKEN_PARSE);
foreach ($tokens as $token) {
    \printf('Line %d: %s (%s) (%s)%s', $token->line, $token->getTokenName(), $token->id, $token->text, PHP_EOL);
}
```

For the rest of the talk we'll keep `token_get_all()` because it is still heavily used 😢

---

# Tokens in PHP

```php
Lorem ipsum

Plop plip

<?php echo "I love ponies";
```

```text
Line 1: T_INLINE_HTML (313) (Lorem ipsum

Plop plip

)
Line 5: T_OPEN_TAG (382) (<?php )
Line 5: T_ECHO (324) (echo)
Line 5: T_WHITESPACE (385) ( )
Line 5: T_CONSTANT_ENCAPSED_STRING (315) ("I love ponies")
String token: ;
```

---

<style scoped>
    code {
      font-size: 0.6em;
    }
</style>

```php
<?php

class Pony
{
    public const PUBLIC = 1;
}
```

```text
Line 1: T_OPEN_TAG (382) (<?php
)
Line 2: T_WHITESPACE (385) (
)
Line 3: T_CLASS (364) (class)
Line 3: T_WHITESPACE (385) ( )
Line 3: T_STRING (311) (Pony)
Line 3: T_WHITESPACE (385) (
)
String token: {
Line 4: T_WHITESPACE (385) (
    )
Line 5: T_PUBLIC (358) (public) <---------------------------------- Difference between the keyword "public"
Line 5: T_WHITESPACE (385) ( )
Line 5: T_CONST (344) (const)
Line 5: T_WHITESPACE (385) ( )
Line 5: T_STRING (311) (PUBLIC) <---------------------------------- and the constant name "PUBLIC"
Line 5: T_WHITESPACE (385) ( )
String token: =
Line 5: T_WHITESPACE (385) ( )
Line 5: T_LNUMBER (309) (1)
String token: ;
Line 5: T_WHITESPACE (385) (
)
String token: }
```

---

# Example

Using tokens, let's check if a class name respects upper camel case (`MyClass`).

```php
foreach ($tokens as $i => $token) {
    if (FALSE === \is_array($token)) {
        continue;
    }

    if (T_CLASS !== $token[0]) {
        continue;
    }

    $classNameToken = $tokens[$i + 2];
    $className = $classNameToken[1];
    $line = $classNameToken[2];

    if (!isUpperCamelCase($className)) {
        printf('Class name "%s" should be in upper camel case at line "%d".%s', $className, $line, PHP_EOL);
    }
}
```

---

# So PHP Tokens..

- needs a lot of helpers to work with.
- intuitive? 🤔 not really...
  But it is better with the [PhpToken class](https://www.php.net/manual/fr/class.phptoken.php) 🥰🥰🥰
- [PHP_CodeSniffer (PHPCS)](https://github.com/squizlabs/PHP_CodeSniffer) is mainly based on them. 👏👏
- scope is limited to a file: check inheritance etc..?

### 🤓 Practice to have fun 🤓

Try to detect and ban code like `$a = $a + 1;`

---

# Abstract Syntax Tree (AST)

🧐

---

# Abstract Syntax Tree (AST):question: 🤔

TL;DR

> a data structure
> to represent the structure of code

<!--
The syntax is "abstract" in the sense that
it does not represent every detail appearing in the real syntax,
but rather just the structural or content-related details.

For instance, grouping parentheses are implicit in the tree structure,
so these do not have to be represented as separate nodes. Likewise,
a syntactic construct like an if-condition-then expression may be denoted by means of a single node with three branches.
-->


---

# AST:question: 🤔

<pre><code>(2 * 3) + 4</code></pre>

![bg right contain](assets/ast/simple-hand-written.png)

<!--
The syntax is "abstract" in the sense that
it does not represent every detail appearing in the real syntax,
but rather just the structural or content-related details.

For instance, grouping parentheses are implicit in the tree structure,
so these do not have to be represented as separate nodes. Likewise,
a syntactic construct like an if-condition-then expression may be denoted by means of a single node with three branches.
-->

---

# Parser to generate the AST 🤓

From userland:

- [php-ast extension](https://github.com/nikic/php-ast): Expose PHP 7 abstract syntax tree
- [nikic/php-parser](https://github.com/nikic/PHP-Parser): A PHP parser written in PHP 👈 :elephant:
- [microsoft/tolerant-php-parser](https://github.com/microsoft/tolerant-php-parser): An early-stage PHP parser designed for IDE usage scenarios

<!--
The parsing stage takes the token stream from the Lexer as input and outputs an Abstract Syntax Tree (AST).

The parser has two jobs:

- verifying the validity of the token order by attempting to match them against any one of the grammar rules defined in its grammar file. This ensures that valid language constructs are being formed by the tokens in the token stream
- generating the AST, which is a tree view of the source code that will be used during the next stage (compilation)

The parser is generated with Bison via the zend_language_parser.y (BNF) grammar file. PHP uses a LALR(1) (look ahead, left-to-right) context-free grammar. The look ahead part simply means that the parser is able to look n tokens ahead (1, in this case) to resolve any ambiguities it may encounter whilst parsing. The left-to-right part means that it parses the token stream from left-to-right.

We can view a form of the AST produced by the parser using the php-ast extension. This extension performs a few transformations upon the AST, preventing it from being directly exposed to PHP developers. This is done for a couple of reasons:

- the AST is not particularly “clean” to work with (in terms of consistency and general usability)
- the abstraction of the internal AST means that changes can be freely applied to it without risk breaking compatibility for PHP developers
-->

---

# Some Nodes used in the AST by [nikic/php-parser](https://github.com/nikic/PHP-Parser)

- `Const`: `const`
- `Stmt\Else`: `else`
- `Stmt\Foreach`: `foreach`
- `Stmt\Function`: `function`
- `Expr\BinaryOp\Equal`: `==`
- `Expr\BinaryOp\Identical`: `===`
- `Expr\Variable`: `$foo`
- `Scalar\LNumber`: `42` (literal number => integer)
- `Scalar\DNumber`: `42.0` (decimal number => float)
- `Scalar\String`: `"I am string"`
- etc..

<span style="font-size:50%; margin-top: 2em">
See:
<a href="https://github.com/nikic/PHP-Parser/blob/master/grammar/php7.y">PHP 7 grammar written in a pseudo language</a>
</span>

---

# Let's generate some AST using [nikic/php-parser](https://github.com/nikic/PHP-Parser)

```php
<?php

use PhpParser\Error;
use PhpParser\NodeDumper;
use PhpParser\ParserFactory;

$code = '<?php echo "I love ponies";';

$parser = (new ParserFactory)->create(ParserFactory::PREFER_PHP7);
try {
    $ast = $parser->parse($code);
} catch (Error $error) {
    echo "Parse error: {$error->getMessage()}" . PHP_EOL;
    return;
}

echo (new NodeDumper)->dump($ast) . PHP_EOL;
```

---

# Generated AST

```php
<?php

echo "I love ponies";
```

```php
array(
    0: Stmt_Echo(
        exprs: array(
            0: Scalar_String(
                value: I love ponies
            )
        )
    )
)
```

![bg right 50%](assets/ast/echo.svg)


---

<style scoped>
    code {
      font-size: 0.6em;
    }
</style>

# Generated AST

 ```php
 Lorem ipsum

Plop plip

<?php echo "I love ponies";

 ```

```php
array(
    0: Stmt_InlineHTML(
        value: Lorem ipsum

    Plop plip


    )
    1: Stmt_Echo(
        exprs: array(
            0: Scalar_String(
                value: I love ponies
            )
        )
    )
)
```

![bg right 90%](assets/ast/echo-with-html.svg)

---

<style scoped>
    code {
      font-size: 0.6em;
    }
</style>

# Generated AST

 ```php
<?php

$a = (2 * 3) + 4;
 ```

```php
array(
    0: Stmt_Expression(
        expr: Expr_Assign(
            var: Expr_Variable(
                name: a
            )
            expr: Expr_BinaryOp_Plus(
                left: Expr_BinaryOp_Mul(
                    left: Scalar_LNumber(
                        value: 2
                    )
                    right: Scalar_LNumber(
                        value: 3
                    )
                )
                right: Scalar_LNumber(
                    value: 4
                )
            )
        )
    )
)
```

![bg right 90%](assets/ast/operation.svg)


---

<style scoped>
    code {
      font-size: 0.45em;
    }
</style>

# Generated AST

 ```php
<?php

final class Pony
{
    public const PUBLIC = 1;
}
 ```

```php
array(
    0: Stmt_Class(
        attrGroups: array(
        )
        flags: MODIFIER_FINAL (32)
        name: Identifier(
            name: Pony
        )
        extends: null
        implements: array(
        )
        stmts: array(
            0: Stmt_ClassConst(
                attrGroups: array(
                )
                flags: MODIFIER_PUBLIC (1)
                consts: array(
                    0: Const(
                        name: Identifier(
                            name: PUBLIC
                        )
                        value: Scalar_LNumber(
                            value: 1
                        )
                    )
                )
            )
        )
    )
)
```

![bg right 90%](assets/ast/class-public.svg)

---

# Generated AST: a more "complex" code 😅😅😅

<style scoped>
    code {
      font-size: 0.7em;
    }
</style>

```php
<?php

declare(strict_types=1);

namespace App\Acme;

final class Item {

    private string $value;

    public function __construct(string $value)
    {
        $this->value = $value;
    }

    public function __toString(): string
    {
        return $this->value;
    }
}
```

🦣🦣We skip the AST's text version because it is huge 🦣🦣

---

![bg contain](assets/ast/dto.svg)

--- 

# Example

Using the AST, let's check if a class name respects upper camel case (`MyClass`).

```php
use PhpParser\Node\Stmt\Class_;
use PhpParser\NodeTraverser;
use PhpParser\NodeVisitorAbstract;

$traverser = new NodeTraverser();
$traverser->addVisitor(new class extends NodeVisitorAbstract {
    public function enterNode(Node $node) {
        if (!$node instanceof Class_) {
            return;
        }

        if (!isUpperCamelCase($node->name)) {
            printf('Class name "%s" should be in upper camel case at line "%d".%s', $node->name, $node->getStartLine(), PHP_EOL);
        }
    }
});
$traverser->traverse($ast);
```

--- 

# Example

<style scoped>
    code {
      font-size: 0.7em;
    }
</style>

Let's change the AST and generate the modified PHP code to force upper camel case

```php
use PhpParser\Node\Identifier;
use PhpParser\Node\Stmt\Class_;
use PhpParser\NodeTraverser;
use PhpParser\NodeVisitorAbstract;
use PhpParser\PrettyPrinter;

$traverser = new NodeTraverser();
$traverser->addVisitor(new class extends NodeVisitorAbstract {
    public function enterNode(Node $node) {
        if (!$node instanceof Class_) {
            return;
        }

        if (!isUpperCamelCase($node->name)) {
            $node->name = new Identifier(strToCamelCase($node->name), $node->getAttributes());
        }
    }
});
$newAst = $traverser->traverse($ast);

echo (new PrettyPrinter\Standard)->prettyPrintFile($newAst);
```

😍 Soooo simple! 😍

---

# Example/Exercice 😛

Let's add the type to the property! 

<div style="display: flex">

<div style="flex: auto">

What we have:
```php
<?php

namespace App\Acme;

final class A {
    /** @var string $v */
    public $v = 'coucou';
}
```
</div>

<div style="flex: auto">

What we want:
```php
<?php

namespace App\Acme;

final class A {
    public string $v = 'coucou';
}

```
</div>

</div>

---

# Example/Exercice 😛

```php
$traverser = new NodeTraverser();
$traverser->addVisitor(new class extends NodeVisitorAbstract {
    public function enterNode(Node $node) {
        if (!$node instanceof Node\Stmt\Property) {
            return;
        }

        $docComment = DocComment::createFromDocBlock($node->getDocComment()->getText());
        $node->setAttribute('comments', NULL);
        $node->type = new Node\Identifier($docComment->getType());
    }
});
$newAst = $traverser->traverse($ast);

echo (new PrettyPrinter\Standard)->prettyPrintFile($newAst);
```

---

# 🤓 Bonus 🤓

## Did you know you could modify the AST during runtime? 😛

With a **PHP Extension** implementing the `zend_ast_process` hook!

<span style="font-size:50%; margin-top: 2em">
See:
<a href="https://www.phpinternalsbook.com/php7/extensions_design/hooks.html#modifying-the-abstract-syntax-tree-ast">PHP internal book</a>
</span>

(⚠️ <span style="font-size:50%; margin-top: 2em">could lead to crashes or weird behavior without perfect understanding of the AST possibilities </span> ⚠️)

<!--
When PHP 7 compiles PHP code it converts it into an abstract syntax tree (AST) before finally generating Opcodes that are persisted in Opcache.
The zend_ast_process hook is called for every compiled script and allows you to modify the AST after it is parsed and created.

This is one of the most complicated hooks to use, because it requires perfect understanding of the AST possibilities.
Creating an invalid AST here can cause weird behavior or crashes.

It is best to look at example extensions that use this hook:

Google Stackdriver PHP Debugger Extension: https://github.com/GoogleCloudPlatform/stackdriver-debugger-php-extension/blob/master/stackdriver_debugger_ast.c

Based on Stackdriver this Proof of Concept Tracer with AST: https://github.com/beberlei/php-ast-tracer-poc/blob/master/astracer.c
-->

---

# So AST...

😍😍😍

- easier to work with than Tokens 😛
- [PHPStan](https://github.com/phpstan/phpstan), [Psalm](https://github.com/vimeo/psalm), [Rector](https://github.com/rectorphp/rector) etc.. are based on it 😘
- [nikic/php-parser](https://github.com/nikic/PHP-Parser) already provides a lot of features 😍😍😍
    - awesome API
    - intuitive
    - Typed

❤️❤️❤️

---

What we saw 🪚 <span style="font-size: 0.5em;">(ba dum tss 🥁)</span>

- We use the "same" technics as PHP to do it: Tokens and AST 🌲🌴🌳
- We are able to analyse and modify PHP code for simple purposes 🤓
- We are able to contribute to tools like [PHP_CodeSniffer (PHPCS)](https://github.com/squizlabs/PHP_CodeSniffer), [PHPStan](https://github.com/phpstan/phpstan), [Psalm](https://github.com/vimeo/psalm), [Rector](https://github.com/rectorphp/rector) etc..
- We are limited, like PHP, to analyse one file at a time.. 😥 _for the moment_ 😛
- Big up to nikic and everyone who enhanced the php static analysis world 👏👏👏

---

# But wait... that's it? 🥺

Next time we'll go a little bit further:

- how to analyse a whole project 🤓
- how phpstan or psalm or rector works internally 🤓
- and who knows 😏

--- 

# Readings

- [List of PHP Static Analysis Tools](https://github.com/exakat/php-static-analysis-tools)
- [PHP Lexer](https://phpinternals.net/categories/lexer)
- [PHP Parser](https://phpinternals.net/categories/parser)
- [PHP Compiler](https://phpinternals.net/categories/compiler)
- [PHP Opcodes](https://phpinternals.net/categories/opcodes)
- [PHP Tokenizer Book](https://www.php.net/manual/en/book.tokenizer.php)
- [List of Tokens](https://www.php.net/manual/en/tokens.php)
- [PHP RFC: Object-based token_get_all() alternative](https://wiki.php.net/rfc/token_as_object)
- [PHP RFC: Abstract Syntax Tree](https://wiki.php.net/rfc/abstract_syntax_tree)
- [nikic PHP Parser](https://github.com/nikic/PHP-Parser)

---

# Thanks! Any Questions?

![auto](assets/banana-beram.gif)
