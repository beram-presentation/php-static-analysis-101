# An introduction to what's behind PHP static code analysis

I love to use static code analysis tools like PHPStan, Psalm etc..
I also love to know how things work.

So I wondered: how does static code analysis work? What's behind those tools?

In this talk we'll cover the basics of how static code analysis works in the PHP world.

Don't be afraid! No need to have a mathematical background, or a deep knowledge of PHP.

## Installation

Those slides are written in Markdown thanks to [Marp](https://marp.app/)
(a Markdown presentation ecosystem).

I, personally, choose to use the
[official marp-cli Docker image](https://hub.docker.com/r/marpteam/marp-cli).
So the installation process only describe this way.
Nevertheless feel free to use another way :)

Clone this repository and go inside:

```bash
git clone [REPOSITORY_URL] php-static-analysis-101 && cd $_
```

Check the `Makefile` to see the available options.
Default is to run `marp-cli` in server mode:

```bash
make
```
